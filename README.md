## ¿Qué es una función en Python?

Una función es una pieza de código que podemos reutilizar. Las funciones en Python se definen de la siguiente manera:

```
def <name>(<parameters>):
  <code>
```

Es importante considerar la indentación en Python. Un ejemplo de función en Python con parámetro es el siguiente:

```python
name = "Carlos"

def greet(parametro):
  print("Hola", parametro)
```

Para poder usar el dato de la variable dentro de la definición de la función, hacemos la llamada de la función con su nombre, pasándole como `argumento` el dato que queremos acceder.

```python
name = "Carlos"

def greet(parametro):
  print("Hola", parametro)


#llamando a la función por su nombre con el argumento necesario
greet(name)
```

## Retorno de una función en Python

La expresión `return` se usa para regresar un objeto específico (expresión evaluada) y terminar la ejecución de la función.

```python
name = "Carlos"

def greet(parametro):
  return "Hola " + parametro


#llamando a la función por su nombre con el argumento necesario
greeting = greet(name)
print(greeting)

```
El valor que regresa una función es conocido como `closure`.

## Funciones anidadas en Python

Python permite definir funciones dentro de otras funciones:

```python
def sum_power(value1, value2, exponential):
  #se suman los valores a y b
  sum = value1 + value2

  def power(exponential):
    #eleva la suma de valores a un exponencial dado
    return sum ** exponential

  return power(exponential)


#ejecutando la función values para sumar valores y elevar a una potencia
values = sum_power(4, 5, 2)
print(values)
```

## Ejercicio - Función vacía en Python

Define la función `hello` que no realiza ninguna acción.

```python
#void function

``` 


## Ejercicio - Función en Python

Define la función `move` que recibe dos parámetros `speed` y `time`. Esta función retornará la distancia que avanza un carro. El resultado de las comparaciones finales debe ser `True`.

```python
#move function


#driver code
print(move() == "El carro avanza: 24 metros")
print(move(2, 3) == "El carro avanza: 6 metros")
```
